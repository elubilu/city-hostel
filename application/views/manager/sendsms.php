<?php include('header.php') ?>
	<div class="row">
		<div class="col-md-12">
			<h3 class="page-header">Send SMS</h3>
		</div>
	</div>
	<div class="row">
      <div class="col-md-12">
         <ol class="breadcrumb">
            <li><a href="<?php echo base_url('manager/');?>">Dash Board</a> </li>
            <li class="active">Send SMS</li>
         </ol>
      </div>
   	</div>
   	<?php include('messages.php') ?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-info">
				<div class="panel-body">
					<div class="row">
						<div class="col-md-12">
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label> Student ID</label>
										<input type="text" class="form-control" name="" value="">
									</div>	
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label> Student Name</label>
										<input type="text" class="form-control" id="date" name="" value="">
									</div>	
								</div>
							</div>
						</div>
					</div>								
				</div>	
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-info filterable">
				<div class="panel-heading">
					<div class="row">
						<div class="col-md-12">
							<div class="col-md-6">
								<h4>Send SMS to </h4>
							</div>							
							<div class="col-md-6">
								<div class="pull-right m-top-10">
									<button id="filter_button" class="btn btn-warning btn-filter with_print m-top-20" ><i class="fa fa-filter"></i> Filter
									</button>
								</div>
							</div>							
						</div>
					</div>
				</div>
				<div class="panel-body">						
					<div class="row">
						<div class="col-md-12 m-top-15">
							<table class="table table-striped" >
								<thead>

									<tr class="filters">											
										<th>
											<input type="text" class="form-control text-left" placeholder="Name" disabled data-toggle="true">
										</th>
										<th>
											<input type="text" class="form-control" placeholder="Phone Number" disabled>
										</th>
										<th>
											<span>Select</span>
										</th>
										
									</tr>
								</thead>
								
								<tbody>
									<tr>
										<td>name</td>
										<td>0123456789</td>
										<td>
											<div class="checkbox">
												<label>
												  <input type="checkbox"> 
												</label>
											</div>
										</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>	
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-info">
				<div class="panel-body">
					<div class="row">
						<div class="col-md-12">
							<div class="row">
								<div class="col-md-12">
									<div class="form-group">
										<label> Message</label>
										<input type="text" class="form-control" name="" value="">
									</div>	
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<button type="submit" class="btn btn-primary">Send</button>
								</div>
							</div>
						</div>
					</div>								
				</div>	
			</div>
		</div>
	</div>
<?php include('footer.php') ?>